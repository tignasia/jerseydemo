package com.ti.webservices.jersey;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by tomasz on 8/14/2016.
 */
//@XmlRootElement
public class Health {

    private final String status;

    public Health(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
