package com.ti.webservices.jersey;

import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Component
@Path("/health")
public class HealthController {
    @GET
    @Produces({MediaType.APPLICATION_JSON/*,MediaType.APPLICATION_XML*/})
    public Health health() {
        return new Health("Jersey: Up and Running!");
    }
}
