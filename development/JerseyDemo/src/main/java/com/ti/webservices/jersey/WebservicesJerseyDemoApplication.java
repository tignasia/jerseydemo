package com.ti.webservices.jersey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebservicesJerseyDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebservicesJerseyDemoApplication.class, args);
	}
}
