package com.ti.webservices.jersey;


import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyTestConfig extends ResourceConfig {
    public JerseyTestConfig() {
        register(HealthController.class);
    }
}
