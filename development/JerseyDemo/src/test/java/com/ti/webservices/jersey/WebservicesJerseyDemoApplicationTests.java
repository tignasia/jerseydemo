package com.ti.webservices.jersey;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.core.Application;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = Application.class, webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
//@WebAppConfiguration
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = {Application.class,/*TestConfig.class, */JerseyTestConfig.class})
//@WebAppConfiguration
//@IntegrationTest("server.port=8080")
//@RunWith(SpringRunner.class)
//classes = {Application.class,TestConfig.class, JerseyTestConfig.class}
//@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {Application.class,TestConfig.class, JerseyTestConfig.class})

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port=9000")
public class WebservicesJerseyDemoApplicationTests {

	private RestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void health() {
		ResponseEntity<Health> entity =
				restTemplate.getForEntity("http://localhost:8080/health", Health.class);
		assertTrue(entity.getStatusCode().is2xxSuccessful());
        assertTrue(entity.getBody().getStatus().equals("Jersey: Up and Running!"));
	}

}
